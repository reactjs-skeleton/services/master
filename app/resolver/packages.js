`use strict`
const { Validator: v } = require('node-input-validator')
const {hashids, validate} = require(`${__basedir}/app/helper/helper`)

module.exports = {
    Query: { packages: () => ({}) },
    Mutation: { packages: () => ({}) },
    PackagesQuery: {
        get: async () => {
            let Package = await db.m_package.findAll({
                attributes: [ 'id', 'package_name', 'provider_id', [sequelize.col('mpv.provider_name'), 'provider_name'], 'purchase', 'sell' ],
                include: [{ attributes: [], model: db.m_provider, as:'mpv', require: true, where: {statusid: 1} }],
                where: { statusid: 1 },
                raw: true
            })
            Package = hashids.encodeArray(Package, ['id', 'provider_id'])
            return Package
        }
    },
    PackagesMutation: {
        create: async (__, req) => {
            let validator = new v(req, { 
                package_name: 'required',
                provider_id: 'required',
                purchase: 'required|numeric',
                sell: 'required|numeric',
            })
            let matched = await validator.check()
            await validate(matched, 'user-input')
    
            req.provider_id = hashids.decode(req.provider_id)
            let findProvider = await db.m_provider.findOne({ attributes: ['id'], where: [{id: req.provider_id, statusid: 1}] })
            await validate(findProvider, 'user-input')
    
            let save = await db.m_package.create({
                package_name: req.package_name,
                provider_id: req.provider_id,
                purchase: req.purchase,
                sell: req.sell,
                statusid: 1
            })
            await validate(save)
            return {id: hashids.encode(save.id)}
        },
        update: async(__, req) => { 
            let validator = new v(req, { 
                package_id: 'required',
                package_name: 'required',
                provider_id: 'required',
                purchase: 'required|numeric',
                sell: 'required|numeric',
            })
            let matched = await validator.check()
            await validate(matched, 'user-input')
            
            req.package_id = hashids.decode(req.package_id)
            let find = await db.m_package.findOne({
                attributes: ['id'],
                where: { id: req.package_id, statusid: 1 },
                raw: true
            })
            await validate(find, 'user-input')
            
            req.provider_id = hashids.decode(req.provider_id)
            let findProvider = await db.m_provider.findOne({ attributes: ['id'], where: [{id: req.provider_id, statusid: 1}] })
            await validate(findProvider, 'user-input')
            
            let save = db.m_package.update(
                { 
                    package_name: req.package_name,
                    provider_id: req.provider_id,
                    purchase: req.purchase,
                    sell: req.sell,
                },
                { where: { id: req.package_id, statusid: 1 } }
            )
            await validate(save)
            return {id: hashids.encode(save.id)}
        }
    }
}