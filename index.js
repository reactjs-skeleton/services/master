const express = require('express')
const {ApolloServer } = require('apollo-server-express')

global.env = require(`${__dirname}/config/env.json`)
global.__basedir = __dirname

const db = require(`${__dirname}/core/db`)
const models = require(`${__dirname}/core/models`)
const schemaRelation = require(`${__dirname}/app/schema-relation`)
const cors = require('cors')
const typeDefs = require('./app/schema-graphql')
const resolvers = require('./app/resolver')
const authorize = require('./app/middleware/authorization')
const permission = require('./app/graphql-permission')
const { applyMiddleware } = require('graphql-middleware')

const app = {
    start() {
        return new Promise(async (resolve, reject) => { resolve({ db : await db(), models: await models() }) }).then(() => {
            const app = new express()
            const server = new ApolloServer({ typeDefs, resolvers, context: authorize })
            server.schema = applyMiddleware(server.schema, permission)
            
            app.use(cors())
            server.applyMiddleware({app})
            
            app.listen(env.app.port, env.app.host)
            
            schemaRelation()
            return server
        })
    },
}

app.start()