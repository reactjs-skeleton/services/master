`use strict`
const { Validator: v } = require('node-input-validator')
const { validate, commit, hashids } = require(`../helper/helper`)
const Sequelize = require('sequelize')
const bcrypt = require('bcryptjs')
const fs = require('fs');

module.exports = {
    async list(req, res) {
        let account = await db.authentication_user.findAll({
            attributes: [
                'username', [sequelize.col('spd.name'), 'name'],
                [sequelize.col('spd.address'), 'address'],
                [sequelize.col('spd.photo'), 'photo'] 
            ],
            include : [{ model : db.s_personal_data, as: 'spd', attributes : [], where: {statusid: 1}, require : true }],
            where: { statusid: 1 },
            raw: true
        })
        
        let userRoles = await db.authentication_user_role.findAll({
            attributes: [ 'username', 'rolename', [sequelize.col('ar.roledesc'), 'roledesc'] ],
            include: [{ model: db.authentication_role, as: 'ar', attributes: [], where: {statusid: 1}, require: true }],
            where: { statusid: 1 },
            raw: true
        })

        for (index in account) {
            let row = account[index]
            account[index].role = []
            for(let nestedRow of userRoles) {
                if(row.username == nestedRow.username) {
                    account[index].role.push({
                        rolename: nestedRow.rolename,
                        roledesc: nestedRow.roledesc
                    })
                }
            }
        }

        let roles = await db.authentication_role.findAll({ attributes: [ 'id', 'rolename', 'roledesc' ], where: { statusid: 1 }, raw: true })
        roles = hashids.encodeArray(roles, 'id')

        res.json({ status: 1, data: { account: account, roles: roles } })
    },
    async add(req, res) {
        let validator = new v(req.body, { 
            name: 'required',
            username: 'required',
            password: 'required|same:repassword',
            roles: 'required',
        })
        let matched = await validator.check()
        await validate(res, matched, validator.errors)
        
        for(let row of req.body.roles) {
            let findRoles = await db.authentication_role.findOne({ attributes: ['id'], where: [{rolename: row, statusid: 1}], raw: true })
            await validate(res, findRoles, 'Invalid Roles. Please refresh your browser.')
        }
        
        let findUsername = await db.authentication_user.findOne({ attributes: ['id'], where: [{username: req.body.username, statusid: 1}], raw: true })
        await validate(res, !findUsername, 'Invalid Username. Username has been used by another account.')
        
        bcrypt.hash(req.body.password, env.app.secret, async (error, password) => {
            let save = await sequelize.transaction()
            let saveUser = await db.authentication_user.create({
                username: req.body.username,
                password: password,
                create_at: sequelize.fn('NOW'),
                create_by: req.auth.username,
                statusid: 1
            })
            let savePersonalData = await db.s_personal_data.create({
                username: req.body.username,
                name: req.body.name,
                create_at: sequelize.fn('NOW'),
                create_by: req.auth.username,
                statusid: 1
            })
    
            let userRolesData = []
            for(let row of req.body.roles) {
                userRolesData.push({
                    username: req.body.username,
                    rolename: row,
                    create_at: sequelize.fn('NOW'),
                    create_by: req.auth.username,
                    statusid: 1
                })
            }
    
            let saveRoles = await db.authentication_user_role.bulkCreate(userRolesData)
            
            await commit(save, saveUser && savePersonalData && saveRoles)
            res.json({ status: 1 })
        })
    },
    async del(req, res) {
        let validator = new v(req.params, { id: 'required' })
        let matched = await validator.check()
        await validate(res, matched, validator.errors)

        let find = await db.authentication_user.findOne({
            attributes: ['id', 'username', 'password'],
            where: { username: req.params.id, statusid: 1 },
            raw: true
        })
        await validate(res, find, 'Account is not found. Please refresh your browser.')
        
        let save = await sequelize.transaction()
        let deactivate = await db.authentication_user.update(
            { statusid: 0 },
            { where: { id: find.id, statusid: 1 } }
        )
        let createNew = await db.authentication_user.create({
            username: find.username,
            password: find.password,
            create_at: sequelize.fn('NOW'),
            create_by: req.auth.username,
            statusid: 0
        })
        await commit(save, deactivate && createNew)

        res.json({ status: 1 })
    },
    async view(req, res) {
        let validator = new v(req.params, { id: 'required' })
        let matched = await validator.check()
        await validate(res, matched, validator.errors)

        let account = await db.authentication_user.findOne({
            attributes: [
                'username', [sequelize.col('spd.name'), 'name'],
                [sequelize.col('spd.address'), 'address'],
                [sequelize.col('spd.photo'), 'photo']
            ],
            include : [{ model : db.s_personal_data, as: 'spd', attributes : [], where: {statusid: 1}, require : true }],
            where: { username: req.params.id, statusid: 1 },
            raw: true
        })
        await validate(res, account, 'Account is not found. Please refresh your browser.')

        account.photo = account.photo ? true : false
        account.roles = await db.authentication_user_role.findAll({
            attributes: [ ['rolename', 'account_rolename'], [sequelize.col('ar.roledesc'), 'roledesc'] ],
            include: [{ model: db.authentication_role, as: 'ar', attributes: [], where: {statusid: 1}, require: true }],
            where: { username: req.params.id, statusid: 1 },
            raw: true
        })

        let roles = await db.authentication_role.findAll({ attributes: [ 'id', 'rolename', 'roledesc' ], where: { statusid: 1 }, raw: true })
        roles = hashids.encodeArray(roles, 'id')

        res.json({ status: 1, data: { account: account, roles: roles } })
    },
    async picture(req, res) {
        let account = await db.s_personal_data.findOne({
            attributes: [ 'photo' ],
            where: { username: req.params.id, statusid: 1 },
            raw: true
        })
        await validate(res, account, 'Invalid account')
        res.sendFile(`${__basedir}/protected/account/${account.photo}`)
    },
    async savePicture(req, res) {
        let validator = new v( { ...req.files }, {
            picture: 'required',
            'picture.mimetype': 'in:image/jpg,image/jpeg,image/png'
        })
        let matched = await validator.check()
        await validate(res, matched, validator.errors)

        let find = await db.s_personal_data.findOne({
            attributes: ['id', 'username', 'name', 'address', 'photo'],
            where: { username: req.params.id, statusid: 1 },
            raw: true
        })
        await validate(res, find, 'Account is not found. Please refresh your browser.')

        /** Save File (Image) */
        let filename = req.files.picture.filename.split('.')
        let pictureName = `${find.username}.${filename[filename.length - 1]}`

        fs.rename(req.files.picture.file, `${__basedir}/protected/account/${pictureName}`, async (err) => {
            /** Save Data */
            let save = await sequelize.transaction()
            let deactivate = await db.s_personal_data.update(
                { statusid: 0 },
                { where: { id: find.id, statusid: 1 } }
            )
            let createNew = await db.s_personal_data.create({
                username: find.username,
                name: find.name,
                address: find.address,
                photo: pictureName,
                create_by: req.auth.username,
                statusid: 1
            })

            await commit(save, deactivate && createNew)
            res.json({ status: 1 })
        })
    },
    async savePersonalData(req, res) {
        let validator = new v(req.body, { 
            name: 'required',
            address: 'required',
        })
        let matched = await validator.check()
        await validate(res, matched, validator.errors)
        
        let find = await db.s_personal_data.findOne({
            attributes: ['id', 'username', 'name', 'address', 'photo'],
            where: { username: req.params.id, statusid: 1 },
            raw: true
        })
        await validate(res, find, 'Account is not found. Please refresh your browser.')
        
        let save = await sequelize.transaction()
        let deactivate = await db.s_personal_data.update({ statusid: 0 }, { where: { id: find.id, statusid: 1 } })
        let create = await db.s_personal_data.create({
            username: req.params.id,
            name: req.body.name,
            address: req.body.address,
            photo: find.photo,
            create_by: req.auth.username,
            statusid: 1
        })
        await commit(save, create && deactivate)
        res.json({ status: 1 })
    },
    async saveRoles(req, res) {
        let validator = new v(req.body, { roles: 'required' })
        let matched = await validator.check()
        await validate(res, matched, validator.errors)
        
        let find = await db.authentication_user.findOne({
            attributes: ['id'], where: { username: req.params.id, statusid: 1 },
            raw: true
        })
        await validate(res, find, 'Account is not found. Please refresh your browser.')

        if(typeof req.body.roles == 'string') {
            let role = req.body.roles
            req.body.roles = []
            req.body.roles.push(role)
        }

        for(let row of req.body.roles) {
            let findRoles = await db.authentication_role.findOne({ attributes: ['id'], where: [{rolename: row, statusid: 1}], raw: true })
            await validate(res, findRoles, 'Invalid Roles. Please refresh your browser.')
        }
        let save = await sequelize.transaction()
        
        let deactivate = await db.authentication_user_role.update({ statusid: 0 }, { where: { username: req.params.id, statusid: 1 } })
        
        let userRolesData = []
        for(let row of req.body.roles) {
            userRolesData.push({
                username: req.params.id,
                rolename: row,
                create_at: sequelize.fn('NOW'),
                create_by: req.auth.username,
                statusid: 1
            })
        }
        let create = await db.authentication_user_role.bulkCreate(userRolesData)

        await commit(save, create && deactivate)
        res.json({ status: 1 })
    },
    async resetPassword(req, res) {
        let validator = new v(req.body, {
            password: 'required',
            repassword: 'required|same:repassword',
        })
        let matched = await validator.check()
        await validate(res, matched, validator.errors)
        
        let find = await db.authentication_user.findOne({
            attributes: ['id'], where: { username: req.params.id, statusid: 1 },
            raw: true
        })
        await validate(res, find, 'Account is not found. Please refresh your browser.')

        bcrypt.hash(req.body.password, env.app.secret, async (error, password) => {
            let save = await sequelize.transaction()
            
            let deactivate = await db.authentication_user.update({ statusid: 0 }, { where: { username: req.params.id, statusid: 1 } })
            let create = await db.authentication_user.create({
                username: req.params.id,
                password: password,
                create_by: req.auth.username,
                statusid: 1
            })
            await commit(save, deactivate && create)

            res.json({ status: 1 })
        })
    },
    async resetUsername(req, res) {
        let validator = new v(req.body, { username: 'required' })
        let matched = await validator.check()
        await validate(res, matched, validator.errors)

        let find = await db.authentication_user.findOne({
            attributes: ['id', 'username', 'password'], where: { username: req.params.id, statusid: 1 },
            raw: true
        })
        await validate(res, find, 'Account is not found. Please refresh your browser.')
        
        let findUsername = await db.authentication_user.findOne({ 
            attributes: ['id'], 
            where: [{username: req.body.username, id: { [Sequelize.Op.ne]: find.id }, statusid: 1}], 
            raw: true 
        })
        await validate(res, !findUsername, 'Invalid Username. Username has been used by another account.')

        let save = await sequelize.transaction()
        let deactivate = await db.authentication_user.update({ statusid: 0 }, { where: { username: req.params.id, statusid: 1 } })
        let saveUser = await db.authentication_user.create({
            username: req.body.username,
            password: find.password,
            create_by: req.auth.username,
            statusid: 1
        })
        let savePersonalData = await db.s_personal_data.update(
            { username: req.body.username },
            { where: {username: req.params.id, statusid: 1} }
        )
        let saveUserRole = await db.authentication_user_role.update(
            { username: req.body.username },
            { where: {username: req.params.id, statusid: 1} }
        )
        
        await commit(save, saveUser && savePersonalData && saveUserRole && deactivate)
        res.json({ status: 1, username: req.body.username })
    }
}