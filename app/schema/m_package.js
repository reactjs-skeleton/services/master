`use strict`;

const Sequelize = require(`sequelize`);

module.exports = (db) => {
    return db.define(`m_package`, {
        provider_id : {
            type      : Sequelize.INTEGER,
            allowNull : false
        },
        package_name : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        purchase : {
            type      : Sequelize.INTEGER,
            allowNull : false
        },
        sell : {
            type      : Sequelize.INTEGER,
            allowNull : false
        },
        statusid : {
            type      : Sequelize.SMALLINT,
            allowNull : false
        },
    }, {
        schema: 'counter',
        freezeTableName : true,
        createdAt       : false,
        updatedAt       : false
    });
};