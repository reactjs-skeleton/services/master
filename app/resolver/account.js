`use strict`
const bcrypt = require('bcryptjs')
const { Validator: v } = require('node-input-validator')
const {hashids, validate, commit} = require(`${__basedir}/app/helper/helper`)

module.exports = {
    Query: { account: () => ({}) },
    Mutation: { account: () => ({}) },
    AccountQuery: {
        get: async (__, req, ctx) => {
            let account = await db.authentication_user.findAll({
                attributes: [
                    'username', [sequelize.col('spd.name'), 'name'],
                    [sequelize.col('spd.address'), 'address'],
                ],
                include : [{ model : db.s_personal_data, as: 'spd', attributes : [], where: {appid: ctx.req.auth.app.id, statusid: 1}, require : true }],
                where: { appid: ctx.req.auth.app.id, statusid: 1 },
                raw: true
            })
            
            let userRoles = await db.authentication_user_role.findAll({
                attributes: [ 'username', 'rolename', [sequelize.col('ar.roledesc'), 'roledesc'] ],
                include: [{ model: db.authentication_role, as: 'ar', attributes: [], where: {appid: ctx.req.auth.app.id, statusid: 1}, require: true }],
                where: { appid: ctx.req.auth.app.id, statusid: 1 },
                raw: true
            })

            for (index in account) {
                let row = account[index]
                account[index].roles = []
                for(let nestedRow of userRoles) {
                    if(row.username == nestedRow.username) {
                        account[index].roles.push({
                            rolename: nestedRow.rolename,
                            roledesc: nestedRow.roledesc
                        })
                    }
                }
            }

            return account
        },
        roles: async (__, req, ctx) => {
            let roles = await db.authentication_role.findAll({ 
                attributes: [ 'id', 'rolename', 'roledesc' ], 
                where: { appid: ctx.req.auth.app.id, statusid: 1 }, 
                raw: true 
            })
            roles = hashids.encodeArray(roles, 'id')
            return roles
        }
    },
    AccountMutation: {
        create: async (__, req, ctx) => {
            let validator = new v(req, { 
                name: 'required',
                username: 'required',
                password: 'required|same:repassword',
                roles: 'required|array',
            })
            let matched = await validator.check()
            await validate(matched, 'user-input')
            
            for(let row of req.roles) {
                let findRoles = await db.authentication_role.findOne({ 
                    attributes: ['id'], 
                    where: [{appid: ctx.req.auth.app.id, rolename: row, statusid: 1}], 
                    raw: true 
                })
                await validate(findRoles, 'user-input')
            }
            
            let findUsername = await db.authentication_user.findOne({ 
                attributes: ['id'], 
                where: [{appid: ctx.req.auth.app.id, username: req.username, statusid: 1}], 
                raw: true 
            })
            await validate(!findUsername, 'user-input')
            
            const password = await bcrypt.hash(req.password, env.app.secret)
            
            let save = await sequelize.transaction()
            let saveUser = await db.authentication_user.create({
                appid: ctx.req.auth.app.id, 
                username: req.username,
                password: password,
                create_at: sequelize.fn('NOW'),
                create_by: ctx.req.auth.username,
                statusid: 1
            })
            let savePersonalData = await db.s_personal_data.create({
                appid: ctx.req.auth.app.id, 
                username: req.username,
                name: req.name,
                create_at: sequelize.fn('NOW'),
                create_by: ctx.req.auth.username,
                statusid: 1
            })
    
            let userRolesData = []
            for(let row of req.roles) {
                userRolesData.push({
                    appid: ctx.req.auth.app.id, 
                    username: req.username,
                    rolename: row,
                    create_at: sequelize.fn('NOW'),
                    create_by: ctx.req.auth.username,
                    statusid: 1
                })
            }

            let saveRoles = await db.authentication_user_role.bulkCreate(userRolesData)
            
            await commit(save, saveUser && savePersonalData && saveRoles)
            return {status: 1}
        },
        update: async(__, req, ctx) => {

        },
        delete: async (__, req, ctx) => {

        }
    }
}