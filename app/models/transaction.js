`use strict`
const { Validator: v } = require('node-input-validator')
const { validate, hashids, commit } = require(`../helper/helper`)
const Sequelize = require('sequelize')
const moment = require('moment')

module.exports = {
    async list(req, res) {
        let transaction = await db.t_transaction.findAll({
            attributes: [ 
                'id', 'transaction_date', 'package_id', 
                [sequelize.col('mpc.package_name'), 'package_name'],
                [sequelize.col('tc.email'), 'customer_email'],
                [sequelize.col('tc.name'), 'customer_name'],
                [sequelize.col('tc.phone'), 'customer_phone'],
                [sequelize.col('mpc.purchase'), 'purchase'],
                [sequelize.col('mpc.sell'), 'sell'],
            ],
            include: [
                { model: db.t_customer, as:'tc', where: {statusid: 1}, require: true },
                { model: db.m_package, as:'mpc', where: {statusid: 1}, require: true },
            ],
            where: { statusid: 1 },
            raw: true
        })
        
        let packages = await db.m_package.findAll({
            attributes: [ 'id', 'package_name', [sequelize.col('mpv.provider_name'), 'provider_name'] ],
            include: [{ model: db.m_provider, as:'mpv', require: true, where: {statusid: 1} }],
            where: { statusid: 1 },
            raw: true
        })

        transaction = hashids.encodeArray(transaction, ['id', 'package_id'])
        packages = hashids.encodeArray(packages, ['id'])

        for await (let row of transaction) {
            row.transaction_date_text = moment.utc(row.transaction_date).format('DD MMM YYYY')
        }
        
        res.json({ status: 1, data: { transaction: transaction, packages: packages} })
    },
    async findCustomer(req, res) {
        let customer = await db.t_customer.findAll({ 
            attributes: [ 'id', 'name', 'phone' ], 
            where: { 
                statusid: 1, 
                $and: Sequelize.where(Sequelize.fn('lower', sequelize.col('name')), {[Sequelize.Op.like]: `%${req.query.term}%`}) }, 
            raw: true 
        })
        customer = hashids.encodeArray(customer, 'id')

        let result = []
        for(let row of customer) {
            result.push({ id: row.id, text: `${row.name} (${row.phone})` })
        }

        res.json({ status: 1, data: result })
    },
    async add(req, res) {
        let validator = new v(req.body, { 
            customer_id: 'required',
            package_id: 'required',
            transaction_date: 'required',
        })
        let matched = await validator.check()
        await validate(res, matched, validator.errors)

        req.body.package_id = hashids.decode(req.body.package_id)
        let findPackage = await db.m_package.findOne({ attributes: ['id'], where: [{id: req.body.package_id, statusid: 1}] })
        await validate(res, findPackage, 'Invalid Package. Please refresh your browser.')
        
        req.body.customer_id = hashids.decode(req.body.customer_id)
        let findCustomer = await db.t_customer.findOne({ attributes: ['id', 'email'], where: [{id: req.body.customer_id, statusid: 1}] })
        await validate(res, findCustomer, 'Invalid Customer. Please refresh your browser.')

        let save = await db.t_transaction.create({
            customer_id: findCustomer.email,
            package_id: req.body.package_id,
            transaction_date: req.body.transaction_date,
            create_by: req.auth.username,
            statusid: 1
        })
        await validate(res, save, 'An error occured while saving package\'s data. Please try again.')
        res.json({ status: 1 })
    },
    async del(req, res) {
        let validator = new v(req.params, { id: 'required' })
        let matched = await validator.check()
        await validate(res, matched, validator.errors)
        
        req.params.id = hashids.decode(req.params.id)
        let find = await db.t_transaction.findOne({
            attributes: ['customer_id', 'package_id', 'transaction_date'],
            where: { id: req.params.id, statusid: 1 },
            raw: true
        })
        await validate(res, find, 'Transaction is not found')

        let save = await sequelize.transaction()
        let deactive = db.t_transaction.update(
            { statusid: 0 },
            { where: { id: req.params.id, statusid: 1 } }
        )
        let createNew = await db.t_transaction.create({
            customer_id: find.customer_id,
            package_id: find.package_id,
            transaction_date: find.transaction_date,
            create_by: req.auth.username,
            statusid: 0
        })
        await commit(save, deactive && createNew)

        await validate(res, save, 'An error occured while deleting transaction\'s data. Please try again.')
        res.json({ status: 1 })
    }
}