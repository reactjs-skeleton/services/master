`use strict`;

const Sequelize = require(`sequelize`);

module.exports = (db) => {
    return db.define(`m_provider`, {
        provider_name : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        statusid : {
            type      : Sequelize.SMALLINT,
            allowNull : false
        },
    }, {
        schema: 'counter',
        freezeTableName : true,
        createdAt       : false,
        updatedAt       : false
    });
};