`use strict`
const { Validator: v } = require('node-input-validator')
const {hashids, validate} = require(`${__basedir}/app/helper/helper`)

module.exports = {
    Query: { provider: () => ({}) },
    Mutation: { provider: () => ({}) },
    ProviderQuery: {
        get: async () => {
            let provider = await db.m_provider.findAll({
                attributes: ['id', 'provider_name' ],
                where: { statusid: 1 },
                order: ['provider_name'],
                raw: true
            })
            provider = hashids.encodeArray(provider, 'id')
            return provider
        }
    },
    ProviderMutation: {
        create: async (__, req) => {
            let validator = new v(req, { provider_name: 'required' })
            let matched = await validator.check()
            await validate(matched, 'user-input')

            let save = await db.m_provider.create({
                provider_name: req.provider_name,
                statusid: 1
            })
            await validate(save)
            return {id: hashids.encode(save.id)}
        },
        update: async (__, req) => {
            let validator = new v(req, { id: 'required', provider_name: 'required' })
            let matched = await validator.check()
            await validate(matched, 'user-input')
            
            req.id = hashids.decode(req.id)
            let find = await db.m_provider.findOne({ attributes: ['id'], where: { id: req.id, statusid: 1 }, raw: true })
            await validate(find, 'user-input')
            
            let save = db.m_provider.update(
                { provider_name: req.provider_name },
                { where: { id: req.id, statusid: 1 } }
            )
            await validate(save)
            return { status: 1 }
        },
        delete: async (__, req) => {
            let validator = new v(req, { id: 'required' })
            let matched = await validator.check()
            await validate(matched, 'user-input')
            
            req.id = hashids.decode(req.id)
            let find = await db.m_provider.findOne({ attributes: ['id'], where: { id: req.id, statusid: 1 }, raw: true })
            await validate(find, 'user-input')

            let save = db.m_provider.update(
                { statusid: 0 },
                { where: { id: req.id, statusid: 1 } }
            )
            await validate(save)
            return { status: 1 }
        }
    }
}