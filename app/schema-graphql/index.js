const { gql } = require('apollo-server-express')

const main = gql`
    scalar JSON
    scalar JSONObject
    scalar Date
    scalar DateTime
    type ProviderQuery {
        get: [JSONObject]
    }
    type ProviderMutation {
        create(provider_name: String!): JSONObject
        update(id: String!, provider_name: String!): JSONObject
        delete(id: String!): JSONObject
    }
    type PackagesQuery {
        get: [JSONObject]
    }
    type PackagesMutation {
        create(package_name: String!, provider_id: String!, purchase: Int!, sell: Int!): JSONObject
        update(package_id: String!, package_name: String!, provider_id: String!, purchase: Int!, sell: Int!): JSONObject
        delete(id: String!): JSONObject
    }
    type CustomerQuery {
        get: [JSONObject]
    }
    type CustomerMutation {
        create(name: String!, phone: String!, email: String!, address: String!): JSONObject
        update(id: String!, name: String!, phone: String!, email: String!, address: String!): JSONObject
        delete(id: String!): JSONObject
    }
    type AccountQuery {
        get: [JSONObject]
        roles: [JSONObject]
    }
    type AccountMutation {
        create(username: String!, name: String!, address: String!, password: String!, repassword: String!, roles: [String]!): JSONObject
        update(username: String!, name: String!, address: String!, password: String!, repassword: String!): JSONObject
        delete(id: String!): JSONObject
    }
    type Query {
        userinfo: JSONObject
        provider: ProviderQuery
        packages: PackagesQuery
        customer: CustomerQuery
        account: AccountQuery
    }
    type Mutation {
        authenticate(username: String!, password: String!): JSONObject
        provider: ProviderMutation
        packages: PackagesMutation
        customer: CustomerMutation
        account: AccountMutation
    }
`

module.exports = [main]