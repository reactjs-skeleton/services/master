`use strict`
const { AuthenticationError, ApolloError, ForbiddenError, UserInputError } = require('apollo-server-express')
const helper = {
    isNotEmpty : (obj) => {
        for(var prop in obj) {
            if(obj.hasOwnProperty(prop)) return true
        }
        return false
    },
    isEmpty : (obj) => { return !helper.isNotEmpty(obj) },
    hashids: {
        encode: (id) => {
            const Hashids = require('hashids/cjs')
            const salt = process.env.HASHIDS_SALT ? process.env.HASHIDS_SALT : 'please create salt at .env'
    
            const hashid = new Hashids(salt, 12)

            return hashid.encode(id)
        },
        encodeObject(data, index) {
            const Hashids = require('hashids/cjs')
            const salt = process.env.HASHIDS_SALT ? process.env.HASHIDS_SALT : 'please create salt at .env'
    
            const hashid = new Hashids(salt, 12)
            
            for(let row of index) {
                data[row] = hashid.encode(data[row])
            }
            
            return data
        },
        encodeArray(data, index = null) {
            const Hashids = require('hashids/cjs')
            const salt = process.env.HASHIDS_SALT ? process.env.HASHIDS_SALT : 'please create salt at .env'
    
            const hashid = new Hashids(salt, 12)

            if(index) {
                for(let row of data) {
                    if(Array.isArray(index)) {
                        for(rowIndex of index) {
                            row[rowIndex] = hashid.encode(row[rowIndex])
                        }
                    } else {
                        row[index] = hashid.encode(row[index])
                    }
                }
            } else {
                for(let index in data) {
                    data[index] = hashid.encode(data[index])
                }
            }

            return data
        },
        decode: (encodeString) => {
            const Hashids = require('hashids/cjs')
            const salt = process.env.HASHIDS_SALT ? process.env.HASHIDS_SALT : 'please create salt at .env'
            
            const hashid = new Hashids(salt, 12)
            return hashid.decode(encodeString)[0]
        },
        decodeArray(data, index = null) {
            const Hashids = require('hashids/cjs')
            const salt = process.env.HASHIDS_SALT ? process.env.HASHIDS_SALT : 'please create salt at .env'
    
            const hashid = new Hashids(salt, 12)

            if(index) {
                for(let row of data) {
                    if(Array.isArray(index)) {
                        for(rowIndex of index) {
                            row[rowIndex] = hashid.decode(row[rowIndex])[0]
                        }
                    } else {
                        row[index] = hashid.decode(row[index])[0]
                    }
                }
            } else {
                for(let index in data) {
                    data[index] = hashid.decode(data[index])[0]
                }
            }

            return data
        },
        decodeObject(data, index) {
            const Hashids = require('hashids/cjs')
            const salt = process.env.HASHIDS_SALT ? process.env.HASHIDS_SALT : 'please create salt at .env'
    
            const hashid = new Hashids(salt, 12)
            
            for(let row of index) {
                data[row] = hashid.decode(data[row])[0]
            }
            
            return data
        },
    },
    validate(condition, code) {
        return new Promise((resolve, reject) => {
            if(condition) { resolve() }
            else {
                switch(code) {
                    case 'authentication':
                        throw new AuthenticationError('Authentication Failed.')
                    case 'forbidden':
                        throw new ForbiddenError('Forbidden Access.')
                    case 'user-input':
                        throw new UserInputError('Invalid Input.')
                    default:
                        throw new ApolloError('An error occured. Please try again later')
                }
            }
        })
    },
    commit(transaction, condition) {
        return new Promise((resolve, reject) => {
            if(condition) { 
                transaction.commit() 
            } else { 
                transaction.rollback() 
            }
            resolve()
        })
    },
    dateParse(val, format = 'Y-m-d') {
        format = format.trim()
        const monthNames = [ 'January', 'February', 'March', 'April', 'May', 
            'June', 'July', 'August', 'September', 'October', 'November', 'December' ]
        const dayNames = [ 'Senin', 'Selasa', 'Rabu', 'Kamis', "Jum'at", 'Sabtu', 'Minggu' ]
        const date = new Date (Date.parse(val))
        const dateSplit = {
            'D' : dayNames[date.getDay() - 1],
            'd' : date.getDate() >= 10 ? date.getDate() : '0' + date.getDate(),
            'm' : date.getMonth() + 1 >= 10 ? date.getMonth() + 1 : '0' + (date.getMonth() + 1),
            'M' : monthNames[date.getMonth()],
            'Y' : date.getFullYear(),
        }
        let response = ''
        for(let row of format) {
            response += dateSplit[row] ? dateSplit[row] : row
        }
        return response
    },
}

module.exports = { ... helper }