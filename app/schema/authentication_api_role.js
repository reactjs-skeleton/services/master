`use strict`;

const Sequelize = require(`sequelize`);

module.exports = (db) => {
    return db.define(`authentication_api_role`, {
        appid : {
            type      : Sequelize.INTEGER,
            allowNull : false
        },
        apiname : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        rolename : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        create_at : {
            type      : Sequelize.DATE,
            allowNull : false,
            defaultValue: Sequelize.fn('NOW')
        },
        create_by : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        statusid : {
            type      : Sequelize.SMALLINT,
            allowNull : false,
            defaultValue: 1
        },
    }, {
        schema: 'authentication',
        freezeTableName : true,
        createdAt       : false,
        updatedAt       : false
    });
};