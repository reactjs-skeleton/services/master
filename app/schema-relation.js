'use strict';
module.exports = () => {
    /** Menu & Label */
    db.s_label_menu.hasMany(db.s_menu, {foreignKey : 'id'});
    db.s_menu.belongsTo(db.s_label_menu, {foreignKey : 'label_id'});

    /** Master Data */
    db.m_package.belongsTo(db.m_provider, { as: 'mpv', foreignKey : 'provider_id'});
    db.m_provider.hasMany(db.m_package, { as: 'mpc', foreignKey : 'id'});
    
    /** User & Authentication */
    db.authentication_user.hasOne(db.s_personal_data, {as: 'spd', foreignKey : 'username', sourceKey : 'username'});
    db.s_personal_data.belongsTo(db.authentication_user, {as: 'au', foreignKey : 'username'});

    /** Roles */
    db.authentication_role.hasMany(db.authentication_user_role, { as: 'aur', foreignKey : 'rolename'});
    db.authentication_user_role.belongsTo(db.authentication_role, { as: 'ar', foreignKey : 'rolename', targetKey: 'rolename'});

    /** Transaction */
    db.t_customer.hasMany(db.t_transaction, { as: 'tt', foreignKey : 'customer_id'});
    db.t_transaction.belongsTo(db.t_customer, { as: 'tc', foreignKey : 'customer_id', targetKey: 'email'});
    db.m_package.hasMany(db.t_transaction, { as: 'tt', foreignKey : 'package_id'});
    db.t_transaction.belongsTo(db.m_package, { as: 'mpc', foreignKey : 'package_id', targetKey: 'id'});
}
