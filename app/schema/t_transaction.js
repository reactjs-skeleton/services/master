`use strict`;

const Sequelize = require(`sequelize`);

module.exports = (db) => {
    return db.define(`t_transaction`, {
        customer_id : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        package_id : {
            type      : Sequelize.INTEGER,
            allowNull : false
        },
        transaction_date : {
            type      : Sequelize.DATE,
            allowNull : false
        },
        create_at : {
            type      : Sequelize.DATE,
            allowNull : false,
            defaultValue: Sequelize.fn('NOW')
        },
        create_by : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        statusid : {
            type      : Sequelize.SMALLINT,
            allowNull : false
        },
    }, {
        schema: 'counter',
        freezeTableName : true,
        createdAt       : false,
        updatedAt       : false
    });
};