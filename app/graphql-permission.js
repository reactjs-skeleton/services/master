const { rule, shield, and, or, not } = require('graphql-shield')
const authenticated = rule({ cache: 'contextual' })(
    async (parent, args, {req}, info) => {
        return req.auth ? true : false
    },
)
const admin = rule({ cache: 'contextual' })(
    async (parent, args, {req}, info) => {
        return req.auth.roles.includes('admin')
    },
)
const superAdmin = rule({ cache: 'contextual' })(
    async (parent, args, {req}, info) => {
        return req.auth.roles.includes('super')
    },
)
module.exports = shield({
    Query: {
        userinfo: authenticated,
        provider: and(authenticated, or(admin, superAdmin))
    },
    Mutation: {
        provider: and(authenticated, or(admin, superAdmin))
    }
})