`use strict`
const { Validator: v } = require('node-input-validator')
const {hashids, validate, commit} = require(`${__basedir}/app/helper/helper`)

module.exports = {
    Query: { customer: () => ({}) },
    Mutation: { customer: () => ({}) },
    CustomerQuery: {
        get: async () => {
            let customer = await db.t_customer.findAll({
                attributes: ['id', 'name', 'email', 'address', 'phone'],
                where: { statusid: 1 },
                raw: true
            })
            customer = hashids.encodeArray(customer, 'id')
            return customer
        }
    },
    CustomerMutation: {
        create: async (__, req, ctx) => {
            let validator = new v(req, { name: 'required', email: 'required|email', address: 'required', phone: 'required|numeric' })
            let matched = await validator.check()
            await validate(matched, 'user-input')

            req.email = req.email.toLowerCase()
            let validateEmail = await db.t_customer.findOne({
                attributes: ['email'],
                where: { email: req.email, statusid: 1 },
                raw: true
            })
            await validate(!validateEmail, 'user-input')

            let validatePhone = await db.t_customer.findOne({
                attributes: ['phone'],
                where: { phone: req.phone, statusid: 1 },
                raw: true
            })
            await validate(!validatePhone, 'user-input')

            let save = await db.t_customer.create({
                name: req.name,
                email: req.email,
                address: req.address,
                phone: req.phone,
                create_by: ctx.req.auth.username,
                statusid: 1
            })
            await validate(save)
            return {id: hashids.encode(save.id)}
        },
        update: async(__, req, ctx) => {
            let validator = new v(req, { id: 'required', name: 'required', email: 'required|email', address: 'required', phone: 'required|numeric' })
            let matched = await validator.check()
            await validate(matched, 'user-input')
            
            req.id = hashids.decode(req.id)
            let find = await db.t_customer.findOne({
                attributes: ['id'],
                where: { id: req.id, statusid: 1 },
                raw: true
            })
            console.log(req, find)
            await validate(find, 'user-input')

            req.email = req.email.toLowerCase()
            let validateEmail = await db.t_customer.findOne({
                attributes: ['email'],
                where: { id: { [Op.ne]: req.id }, email: req.email, statusid: 1 },
                raw: true
            })
            await validate(!validateEmail, 'user-input')
            
            let save = await sequelize.transaction()
            let deactive = db.t_customer.update(
                { statusid: 0 },
                { where: { id: req.id, statusid: 1 } }
            )
            
            let create = await db.t_customer.create({
                name: req.name,
                email: req.email,
                address: req.address,
                phone: req.phone,
                create_by: ctx.req.auth.username,
                statusid: 1
            })

            await commit(save, deactive && create)
            await validate(save)
            return { id: hashids.encode(save.id) }
        },
        delete: async (__, req, ctx) => {
            let validator = new v(req, { id: 'required' })
            let matched = await validator.check()
            await validate(matched, 'user-input')
            
            req.id = hashids.decode(req.id)
            let find = await db.t_customer.findOne({
                attributes: ['email', 'name', 'address', 'phone'],
                where: { id: req.id, statusid: 1 },
                raw: true
            })
            await validate(find, 'user-input')

            let save = await sequelize.transaction()
            let deactive = db.t_customer.update(
                { statusid: 0 },
                { where: { id: req.id, statusid: 1 } }
            )
            let createNew = await db.t_customer.create({
                email: find.email,
                name: find.name,
                address: find.address,
                phone: find.phone,
                create_by: ctx.req.auth.username,
                statusid: 0
            })
            await commit(save, deactive && createNew)
            await validate(save)
            
            return {status: 1}
        }
    }
}