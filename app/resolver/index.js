`use strict`
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const {validate} = require(`${__basedir}/app/helper/helper`)
const {GraphQLJSON, GraphQLJSONObject} = require('graphql-type-json')
const {GraphQLDate, GraphQLDateTime} = require('graphql-iso-date')
const fs = require('fs')

const provider = require('./provider')
const packages = require('./packages')
const customer = require('./customer')
const account = require('./account')

const resolvers = {
    JSON: GraphQLJSON,
    JSONObject: GraphQLJSONObject,
    Date: GraphQLDate,
    DateTime: GraphQLDateTime,
    Query: {
        userinfo: async (__, req, ctx) => {
            let userinfo = await db.s_personal_data.findOne({
                attributes: ['username', 'name', 'address', 'photo'],
                where: { appid: ctx.req.auth.app.id, username: ctx.req.auth.username, statusid: 1 },
                raw: true
            })
            
            
            const resolveMenu = async(rolename = []) => {
                const menu = await db.s_menu.findAll({
                    attributes : [ 'id', 'menu', 'parent_id', 'url', 'icons', 'sequence', [sequelize.col('s_label_menu.label'), 'label'] ],
                    include : [{ model : db.s_label_menu, attributes : [], require : true }],
                    where : { rolename : { [Op.overlap] : rolename }, statusid : 1 },
                    order : ['sequence'],
                    raw: true
                })
            
                let response = [], tempMenu = []
                
                const insertParent = (newMenu, menu) => {
                    for (let x in menu) {
                        if (menu[x]['id'] === newMenu['parent_id']) {
                            menu[x]['child'] = [
                                ... menu[x]['child'] ? menu[x]['child'] : [],
                                {
                                    title       : newMenu.menu,
                                    url         : newMenu.url,
                                    sequence    : newMenu.sequence,
                                    icons       : newMenu.icons
                                }
                            ]
                            break
                        }
                    }
                }
            
                for (let row of menu) {
                    if (row.parent_id) {
                        insertParent(row, tempMenu)
                    } else {
                        tempMenu = [ ... tempMenu, row]
                    }
                }
            
                for (let row of tempMenu) {
                    response[row.label] = {
                        label : row.label,
                        menu : [
                            ... response[row.label] ? response[row.label].menu : [],
                            {
                                title : row.menu,
                                icons : row.icons,
                                url : row.url,
                                sequence : row.sequence,
                                child : row.child ? row.child : undefined
                            }
                        ]
                    }
                }
            
                return { menu: Object.keys(response).map(row => response[row]) }
            }

            const hasPhoto = userinfo.photo ? true : false
            userinfo.photo = hasPhoto ? fs.readFileSync(`${__basedir}/protected/account/${userinfo.photo}`, {encoding: 'base64'}) : false
            userinfo = { ...userinfo, ... await resolveMenu(ctx.req.auth.roles) }

            return userinfo
        }
    },
    Mutation: {
        authenticate: async (__, ctx) => {
            let authApp = await db.application.findOne({ attributes: ['id', 'appname', 'secret'], where: { secret: env.app.secret }, raw: true })
            await validate(authApp, 'authentication')
    
            ctx.password = await bcrypt.hash(ctx.password, env.app.secret)
    
            let user = await db.authentication_user.findOne({ attributes: ['username'], where: { appid: authApp.id, username: ctx.username, password: ctx.password, statusid: 1 } })
            await validate(user, 'authentication')
    
            const token = jwt.sign({username: user.username}, env.app.secret)
            let session = await db.authentication_user_login.create({ appid: authApp.id, username: user.username, token: token })
            await validate(session)
    
            return {token}
        },
    }
}

module.exports = [resolvers, provider, packages, customer, account]