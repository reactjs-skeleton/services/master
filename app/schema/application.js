`use strict`;

const Sequelize = require(`sequelize`);

module.exports = (db) => {
    return db.define(`application`, {
        appname : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        secret : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        statusid : {
            type      : Sequelize.SMALLINT,
            allowNull : false,
            defaultValue: 1
        },
    }, {
        schema: 'authentication',
        freezeTableName : true,
        createdAt       : false,
        updatedAt       : false
    });
};